var SENSOR_TYPE_ICON = {};
SENSOR_TYPE_ICON["default"] = "fa fa-eye";
SENSOR_TYPE_ICON["test"] = "fa fa-tachometer";
SENSOR_TYPE_ICON["pressure"] = "fa fa-download";
SENSOR_TYPE_ICON["temperature"] = "fa fa-thermometer";
SENSOR_TYPE_ICON["humidity"] = "fa fa-tint";
SENSOR_TYPE_ICON["relay"] = "fa fa-microchip";

function getSensorTypeIcon(sType) {
  if (SENSOR_TYPE_ICON[sType]) {
    return SENSOR_TYPE_ICON[sType];
  } else {
    return SENSOR_TYPE_ICON["default"];
  }
}