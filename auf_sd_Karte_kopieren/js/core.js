/**
 * core is a predefined set of routines, handlers, etc. that assures that the basic functionality of the app will work
 */

/**
 * On DOM ready load the page sections first and place the html wrappers in place
 * 
 */

var app;

$(document).ready(function () {
  app = new Application(getHost() + '/api/');
  initAppHandlers();
});

// Control handlers

/**
 * Main method that calls the methods for setting the handlers for specific controls
 * @returns null
 */
function initAppHandlers() {
  initMenuHandlers();
  initSubmenuHandlers();
  initSensorHandlers();
  initIoMatrixHandlers();
  initFormHandlers();
  
  initThemeChanger();
}

/**
 * Method for initializing the main menu
 * @returns {undefined}
 */
function initMenuHandlers() {
  $(document).on("click", "nav.mainmenu .menu_item a", function (event) {
    event.preventDefault();
    let dest = $(this).attr("href");
    if (dest) {
      $(this).parents("nav").find(".selected").removeClass("selected");
      $(this).addClass("selected");

      if ($(this).attr("href") === "connections") {
        // updateConnectionsTable();
      }

      if ($(this).attr("href") === "sensors") {
        // updateConnectionsTable();
        $(".subpages aside").show();
      } else {
        $(".subpages aside").hide();
      }

      // show tab content
      $(".page.active").removeClass("active");
      $("#" + dest + ".page").addClass("active");
    } else {
      let operation = $(this).attr("data-op");
      if (operation === "save") {
        //saveSettings();
      } else if (operation === "simulate") {
        //toggleSimulationMode();
        //toggleSwitch($(".tab_configuration #simulation_toggle"));
      }
    }
  });
}

/**
 * Submenu handlers
 * @returns {undefined}
 */
function initSubmenuHandlers() {
  $(document).on("click", ".submenu .menu_item a", function (event) {
    event.preventDefault();
    let dest = $(this).attr("href");
    if (dest) {
      $(this).parents("nav").find(".selected").removeClass("selected");
      $(this).addClass("selected");
      $('.subpages #' + dest).addClass("active").siblings().removeClass("active");
      if(dest === "type_overview"){
        $(".overview_toolbar").show();
        $(".action_toolbar").hide();
      } else {
        $(".action_toolbar").show();
        $(".overview_toolbar").hide();
      }
    }
  });
}

/**
 * Manages form submit handlers
 * @returns {undefined}
 */
function initFormHandlers() {
  $(document).on("submit", "form", function (event) {
    event.preventDefault();
    const act = $(this).attr("action");
    const subact = $(this).attr("data-action");
    const json = $(this).serializeArray();
    app.formHandler(act, subact, json);
  });

  // sets the data-action to the parent form to specify which button submitted the form
  // in order to determine the right action
  $(document).on("click", "form button", function () {
    $(this).parents("form").attr("data-action", $(this).val());
  });

  $(document).on("click", "button.savesensors", function () {
    app.formHandler("sensors", "save", null);
  });

  $(document).on("click", "button.simulate", function () {
    app.formHandler("sensors", "simulate", null);
  });

}

/**
 * Initializes sensor box handlers
 * @returns {undefined}
 */
function initSensorHandlers() {
  $(document).on("click", ".input_toggle", function () {
    $(this).toggleClass("on");
    const val = $(this).hasClass("on") ? 1 : 0;
    $(this).find("input").val(val);
    // check data-target
    const dt = $(this).attr("data-target");
    if (dt) {
      $("#" + dt).toggleClass("on");
    }

    var dataTarget = $(this).attr("data-target");
    if(dataTarget){
      if($(this).hasClass("on")){
        $("#" + dataTarget).show();
      } else {
        $("#" + dataTarget).hide();
      } 
    }

    // sensor
    if ($(this).parents(".sensor").length > 0) {
      let $obj = $(this).parents(".tab_box.sensor");
      const sensor = $obj.data();
      let s = app.getSensor(sensor.idx);
      const dt = $(this).find("input").attr("name");
      if (dt === "status") {
        s.setStatus(val);
      }
      if (dt === "email") {
        s.setEmail(val);
      }
      if (dt === "snmp") {
        s.setSnmp(val);
      }
      $obj.data(s);
    }

  });
  
  $(".toggler").click(function () {
    $(this).toggleClass("on");
  });

  initToolbarHandlers();
}

/**
 * Handler for toolbar buttons
 * @returns {undefined}
 */
function initToolbarHandlers() {
  // handler for sensor tab - general and tag/nachtmodus - REMOVED
  $(document).on("click", ".box_tab_line .box_tab", function () {
    if (!$(this).hasClass("active")) {
      let $tabBox = $(this).parents(".tab_box");
      let target = $(this).attr("data-target");
      $(this).addClass("active").siblings().removeClass("active");
      $tabBox.find("." + target).addClass("active").siblings().removeClass("active");
    }
  });

  // handler for ON filter - filters all ON sensors (status true)
  $(document).on("click", ".action_toolbar > .action_filter_on", function () {
    let $this = $(this);
    $this.toggleClass("active", function () {
      let $tabsWrapper = $this.parents(".sensors");
      $tabsWrapper.removeClass("filter_off").toggleClass("filter_on");
      $this.siblings(".action_filter_off").removeClass("active");
    });
  });

  // handler for OFF filter - filters all OFF sensors (status false)
  $(document).on("click", ".action_toolbar > .action_filter_off", function () {
    let $this = $(this);
    $this.toggleClass("active", function () {
      let $tabsWrapper = $this.parents(".sensors");
      $tabsWrapper.removeClass("filter_on").toggleClass("filter_off");
      $this.siblings(".action_filter_on").removeClass("active");
    });
  });

  // handler for blocks overfiew
  $(document).on("click", ".overview_toolbar > .overview_blocks", function () {
    let $this = $(this);
    $this.toggleClass("active", function () {
      let $tabsWrapper = $("#type_overview");
      $tabsWrapper.removeClass("list");
      $this.siblings(".overview_list").removeClass("active");
    });
  });

  // handler for list overview
  $(document).on("click", ".overview_toolbar > .overview_list", function () {
    let $this = $(this);
    $this.toggleClass("active", function () {
      let $tabsWrapper = $("#type_overview");
      $tabsWrapper.addClass("list");
      $this.siblings(".overview_blocks").removeClass("active");
    });
  });
}

/**
 * Initialises the handlers on the IO matrix entries
 * @returns {undefined}
 */
function initIoMatrixHandlers() {
  // handler for email and snmp
  $(document).on("click", "#ioMatrix .conn", function () {
    $(this).toggleClass("on");
    const id = $(this).parents(".entry").attr("data-target");
    const $obj = $("#" + id);
    const sensor = $obj.data();
    let s = app.getSensor(sensor.idx);
    const dt = $(this).parent().attr("data-target");
    if (dt === "email") {
      s.setEmail($(this).hasClass("on") ? 1: 0);
    }
    if (dt === "snmp") {
      s.setSnmp($(this).hasClass("on") ? 1: 0);
    }
    $obj.data(s);
  });

  // handler for status
  $(document).on("click", "#ioMatrix .entry .th", function () {
    $(this).parent().toggleClass("on");
    const id = $(this).parents(".entry").attr("data-target");
    const sensor = $("#" + id).data();
    const $obj = $("#" + id);
    let s = app.getSensor(sensor.idx);
    const dt = $(this).attr("data-target");
    let val = $(this).parent().hasClass("on") ? 1: 0;
    s.setStatus(val);
    $obj.data(s);
  });
}
  
// Helper methods

/**
 * Gets the host
 * @returns {unresolved}
 */
function getHost() {
  var http = location.protocol;
  var slashes = http.concat("//");
  var host = slashes.concat(window.location.hostname);
  return host;
}

/**
 * Asyncronously loads content of a file
 * @param {type} url
 * @returns {undefined}
 */
function loadFile(url) {
  var content = null;
  $.ajax({
    async: false,
    method: "POST",
    global: false,
    url: url,
    dataType: "json",
    success: function (data) {
      content = data;
    }
  });
  return content;
}

/**
 * Generic method for saving object to server
 * @param {type} jsonObject
 * @returns {undefined}
 */
function saveToServer(jsonObject) {
  console.log("Saving to server: ", jsonObject);
  if (app) {
    $.ajax({
      url: app.serverIp,
      data: JSON.stringify(jsonObject),
      method: "POST",
      dataType: 'json'
    }).done(function (data) {
      app.logger.info("Data: saving successful!");
    }).fail(function (data, status, error) {
      app.logger.error("Data: saving failed!");
    }).always(function (data) {
      $('.configuration form').removeClass("processing");
      //console.log("always", data);
    });
  }
}

/**
 * Populates the value of the passed field
 * @param {type} field
 * @param {type} value
 * @returns {undefined}
 */
function populateValue(field, value) {
  const dt = $(field).parents(".input_wrapper").attr("data-type");
  switch (dt) {
    case "select":
      $(field).val(value);
      break;

    case "toggle":
      if ($(field).val() === true) {
        if (value !== true || value !== 1) {
          $(field).parents(".input_toggle").click();
        }
      } else {
        if (value === true || value === 1) {
          $(field).parents(".input_toggle").click();
        }
      }
      break;

    case "checkbox":
      $(field).prop('checked', value);
      break;

    case "textarea":
      $(field).val(value);
      break;

    case "input":
      $(field).val(value);
      break;

    default:
      $(field).val(value);
      break;
  }
}

  
// TODO: for removal:  

/**
 * Initialises the theme changer selector in the configuration page
 * @returns {undefined}
 */
function initThemeChanger() {
  // save theme to cookie
  $(document).on("change", "#theme_changer", function () {
    let t = $(this).val();
    if ($(this).val() !== "") {
      $('head link#maintheme').attr("href", "css/themes/" + $(this).val());
      $.cookie("whirleyTheme", $(this).val());
    } else {
      $("head link#customtheme").remove();
      $("head link#maintheme").attr("href", "css/style.css");
      $.removeCookie("whirleyTheme");
    }
  });
}
