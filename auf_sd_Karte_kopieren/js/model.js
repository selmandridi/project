/**
 * Application - main class
 * @type {type}
 */

var noOfSamples = 50;

class Application {
  constructor(host) {
    this.serverIp = host;
    this.sensorFetchInterval = 2000;
    this.uxLoadFromServer = false;
    this.pageLoadIndicator = [];
    this.theme = "";
    this.simulation = false;
    this.settings = [];
    this.sensorTypes = [];
    this.logger = new Logger(false, true, "#ui_logger");

    this.ux = [{"name": "Whirley", "subtitle": "Control Panel Dashboard"}];
    this.loadCookieSettings();
  }

  /**
   * Loads the cookie settings into the app
   * At the moment, used only to keep the selected theme
   * @returns {undefined}
   */
  loadCookieSettings() {
    this.logger.info("Loading the saved settings from cookies");
    let ct = $.cookie("whirleyTheme");
    if (ct) {
      // set theme
      $("head link#maintheme").attr("href", "css/themes/" + ct);
      this.theme = ct;
    }
    // loadUX after cookie settings has been read
    this.loadUX();
  }

  /**
   * Method for loading / initialising UX
   * @returns {undefined}
   */
  loadUX() {
    $(".loader .loader_message").html("loading UX configuration");
    $(".loader").addClass("on");
    this.loadUx();
  }

  /**
   * Method that loads the UX from the local templates
   * @returns {undefined}
   */
  loadUx() {
    const self = this;
    // load from templates
    this.logger.info("Loading UX from local");
    $(".loader .loader_message").removeClass("error").html("UX loaded.<br>Initializing");
    setTimeout(function () {
      self.loadHeader();
      self.showFooter();
    }, 500);
  }

  /**
   * Method for loading the UX provided by the server JSON configuraion
   * @returns {undefined}
   */
  loadUxFromServer() {
    const json = {"config": {"cmd": "ux"}};
    const self = this;
    this.logger.info("Loading UX from the server");
    $.ajax({
      url: self.serverIp,
      method: "POST",
      data: JSON.stringify(json),
      dataType: 'json'
    }).done(function (data) {
      if (!data) {
        self.logger.error("UX not loaded");
        $(".loader .loader_message").addClass("error").html("Service unreachable!<br>Please reload the page");
        return;
      }
      self.ux = data;
      $(".loader .loader_message").removeClass("error").html("UX loaded.<br>Initializing");
      setTimeout(function () {
        self.loadHeader();
        self.showFooter();
      }, 500);
    }).error(function (data, status, error) {
      self.logger.error("UX not loaded: " + error);
      $(".loader .loader_message").addClass("error").html("UX not loaded.<br>Please reload the page");
    }).always(function (data) {
    });
  }

  /**
   * Method for loading the settings from the server
   * Sets the Configuration, Email, SNMP, Time
   * @returns {undefined}
   */
  loadSettings() {
    const json = {"get":["settings"]};
    const self = this;
    this.logger.info("Config: loading settings from server");
    $(".loader .loader_message").html("loading application configuration");
    $(".loader").addClass("on");
    $.ajax({
      url: self.serverIp,
      method: "POST",
      data: JSON.stringify(json),
      dataType: 'json'
    }).done(function (data) {
      if (!data) {
        self.logger.error("Configuration not loaded");
        $(".loader .loader_message").addClass("error").html("Service unreachable!<br>Please reload the page");
        return;
      }
      self.settings = new Settings(data);
    }).error(function (data, status, error) {
      self.logger.error("Configuration not loaded: " + error);
      $(".loader .loader_message").addClass("error").html("Configuration not loaded.<br>Please reload the page");
      self.settings = new Settings();
    }).always(function (data) {
    });
  }

  /**
   * Loads header section
   * If not received in the ux, it loads the defaults from templates
   * @returns {undefined}
   */
  loadHeader() {
    const self = this;
    $(".page_header").addClass("animated fadeInLeft", function () {
      self.logger.info("UX: Header section loaded from local");
      self.loadMenu();
    });
  }

  /**
   * Shows the footer section 
   * @return {undefined}
   */
  showFooter() {
    $(".page_footer").removeClass("hidden").addClass("animated fadeInRight");
  }

  /**
   * Loads the menu template
   * If not received in the ux (no pages object), it loads the defaults from templates
   * @returns {undefined}
   */
  loadMenu() {
    const self = this;
    $(".mainmenu").addClass("animated fadeIn").delay(1000).removeClass("hidden");
    self.logger.info("UX: Menu initialized");
    self.loadContent();
  }

  /**
   * Loads the content section
   * If not received in the ux, it loads the defaults from templates
   * @return {undefined}
   */
  loadContent() {
    const self = this;
    $(".content").removeClass("hidden").addClass("animated fadeInUpBig");
    // load defaults
    // loop through the menu items, and call load methods for each
    self.logger.info("UX: loading pages from local");
    $(".mainmenu .menu_item a").each(function () {
      let pageName = $(this).attr("href");

      // add pages to pageLoadIndicator
      self.pageLoadIndicator.push({name: pageName, loaded: false});

      let active = $(this).hasClass("selected");
      let $page = $("<div>").attr("id", pageName).addClass("page " + pageName);
      $page.load("inc/" + pageName + ".html", function () {
        $(".content").append(this);
        if (active) {
          $(this).addClass("active");
        }
        if ($("body").hasClass("no_overflow")) {
          $("body").delay(2000).queue(function (next) {
            $(this).removeClass("no_overflow");
            $(".loader_wrapper").remove();
            next();
          });
        }
        self.logger.info("* UX: page loaded: " + pageName);
        self.pageLoaded(pageName);
      });
    });
  }

  /**
   * Method that marks that a specified page has been loaded
   * This method also calls the loadData method once the UX is loaded
   * @param {type} pageName
   * @returns {undefined}
   */
  pageLoaded(pageName) {
    let allLoaded = true;
    this.pageLoadIndicator.forEach((page) => {
      if (page.name === pageName) {
        page.loaded = true;
      }
      allLoaded = allLoaded && page.loaded;
    });
    if (allLoaded) {
      this.loadData();
    }
  }

  /**
   * General method for loading and filling in the data
   * @returns {undefined}
   */
  loadData() {
    // set the theme loaded from cookie to the selector
    $("#theme_changer option[value='" + this.theme + "']").prop("selected", true);

    // load settings
    this.loadSettings();

    // load sensors
    this.loadSensors();
  }

  /**
   * Loads the page content - from server ux
   * @param {type} content
   * @param {type} $page
   * @return {undefined}
   */
  loadPageContent(content, $page) {
    const self = this;
    if (content.aside) {
      self.loadPageSidebar(content.aside, $page);
    }

    if (content.sections) {
      content.sections.forEach((key, value) => {
        self.loadPageSection(key);
      });
    }
  }

  /**
   * Loads the page sidebar - from server ux
   * @param {type} aside
   * @param {type} $page
   * @return {undefined}
   */
  loadPageSidebar(aside, $page) {
    const self = this;
    aside.forEach((key, value) => {
      switch (key.type) {
        case "nav":
          self.appendNav(key, $page);
          break;
        default:
          // TODO
          break;
      }
    });
  }

  /**
   * Loads the sidebar filter items - from server UX
   * @param {type} nav
   * @param {type} $page
   * @returns {undefined}
   */
  appendNav(nav, $page) {
    debugger;
    let $aside = $();
    $aside.load("inc/sidebar.html", function () {

      let $li = $aside.find("li").remove();
      nav.items.each((key, value) => {
        let $navItem = $li.clone().html(key.name);
        $aside.find("ul").append($navItem);
      });
    });
    $page.append($aside);
  }

  /**
   * Method that creates and loads the data for the page section
   * @param {type} section
   * @param {type} $page
   * @return {undefined}
   */
  loadPageSection(section, $page) {
    // TODO
  }

  /**
   * Loads sensors JSON from server and calls the processSensors method
   * @returns {undefined}
   */
  loadSensors() {
    $("body").addClass("ui_loaded");
    // load sensors
    const json = {"get":["sensors"]};
    const self = this;
    this.logger.info("Data: loading sensors");
    $(".loader .loader_message").html("loading UX configuration");
    $(".loader").addClass("on");
    $.ajax({
      url: self.serverIp,
      method: "POST",
      data: JSON.stringify(json),
      dataType: 'json'
    }).done(function (data) {
      if (data) {
        self.processSensors(data);
      }
    }).error(function (data, status, error) {
      app.logger.error("Data: Sensors not loaded: " + error);
      $(".loader .loader_message").addClass("error").html("Sensors not loaded.<br>Please reload the page");
    }).always(function (data) {
    });
  }

  /**
   * Process the loaded sensors, draws the ui
   * @param {type} data
   * @returns {undefined}
   */
  processSensors(data) {
    const self = this;
    (data.sensorGroups).forEach((sensorType) => {
      let st = new SensorType(sensorType.name, sensorType.unit);
      (sensorType.sensors).forEach((sensor) => {
        st.sensors.push(new Sensor(sensorType, sensor));
      });
      self.sensorTypes.push(st);
    });
    // console.log("SENSORS", this.sensorTypes);
    app.logger.info("Data: Processing sensors");
    this.createSensors();
  }

  /**
   * Creates the submenu nav elements, panes, and sensors
   * @returns {undefined}
   */
  createSensors() {
    const self = this;
    const $t_submItem = $("#sensors .submenu .menu_item:first-child").clone();
    const $t_sensorsTypePane = $("#sensors .sensors_type").detach();
    const $t_sensor = $t_sensorsTypePane.find(".sensor").remove().clone();

    const $t_matrixGroup = $("#ioMatrix .tgroup").detach();
    const $t_matrixEntry = $t_matrixGroup.find(".trow.entry").detach();

    $("#sensors .submenu .menu_item").remove();

    this.createOverview($t_submItem, $t_sensorsTypePane, $t_sensor);

    this.sensorTypes.forEach((sType, index) => {
      // clone templates
      let $submItem = $t_submItem.clone();
      let $sensorTypePane = $t_sensorsTypePane.clone();
      // handle submenu
      $submItem.find("a").attr("href", "type_" + sType.name);
      $submItem.find("a span").html(sType.name);
      $submItem.find("a i").attr("class", getSensorTypeIcon(sType.name));
      $submItem.find("a").removeClass("selected");
      $sensorTypePane.removeClass("active");
      $("#sensors .submenu ul").append($submItem);

      // panes - groups
      $sensorTypePane.attr("id", "type_" + sType.name);
      $sensorTypePane.attr("data-title", sType.name);
      $sensorTypePane.addClass(sType.name);
      $("#sensors .sensors_wrapper").append($sensorTypePane);

      // io matrix
      let $ioGroup = $t_matrixGroup.clone();
      $ioGroup.addClass(sType.name).find(".caption .th.allspan").html(sType.name);

      // loop through sensors
      sType.sensors.forEach((sensor) => {
        sensor.createUi($t_sensor, sType.name, $sensorTypePane);

        $ioGroup.append(self.createIoMatrixEntry($t_matrixEntry.clone(), sType, sensor));
      });

      // add group to IO matrix
      $("#ioMatrix").append($ioGroup);

    });
  }

  /**
   * Creates the overview pane
   * @param {type} $t_submItem
   * @param {type} $t_sensorsTypePane
   * @param {type} $t_sensor
   * @returns {undefined}
   */
  createOverview($t_submItem, $t_sensorsTypePane, $t_sensor) {
    // clone templates
    let $submItem = $t_submItem.clone();
    let $sensorTypePane = $t_sensorsTypePane.clone();
    // handle submenu
    $submItem.find("a").attr("href", "type_overview");
    $submItem.find("a span").html("overview");
    $submItem.find("a i").attr("class", getSensorTypeIcon("overview"));
    $("#sensors .submenu ul").append($submItem);

    // panes - groups
    $sensorTypePane.attr("id", "type_overview");
    $sensorTypePane.attr("data-title", "overview");
    $sensorTypePane.addClass("overview");
    $("#sensors .sensors_wrapper").append($sensorTypePane);

    let ports = {};
    let $portTemplate = $(".port").detach();
    let $portSensorTemplate = $portTemplate.find(".sensor").detach();

    // loop through sensors
    this.sensorTypes.forEach((sType, index) => {
      sType.sensors.forEach((sensor) => {
        if (!ports[sensor.port]) {
          ports[sensor.port] = [];
        }
        sensor.unit = sType.unit;
        sensor.type = sType.name;
        ports[sensor.port].push(sensor);
      });
    });

    $.each(ports, function (port, data) {
      let $portElem = $portTemplate.clone();
      $portElem.find(".port_name").html(port);
      data.forEach((sensor, i) => {
        // sensor.createOverviewUi($t_sensor, sType.name, $sensorTypePane);
        let $pSensor = $portSensorTemplate.clone();
        $pSensor.attr("id", "ov-" + sensor.idx);
        $pSensor.find(".name").html(sensor.name);
        $pSensor.find(".unit").html(sensor.unit);
        $pSensor.find(".name").prepend('<i class="name_icon ' + SENSOR_TYPE_ICON[sensor.type] + '"></i>');
        $pSensor.find(".value_unit .caption").after('<i class="vu_icon ' + SENSOR_TYPE_ICON[sensor.type] + '"></i>');
        $pSensor.find(".value").html("NA");
        $portElem.find(".port_data").append($pSensor);
      });
      app.logger.info("* Data: Port overview loaded: " + port);
      $sensorTypePane.append($portElem);
    });
  }

  /**
   * Creates one IO matrix entry for the provided sensor, 
   * based on the provided template for the row
   * @param {type} $entry
   * @param {type} sType
   * @param {type} sensor
   * @returns Sensor UI element
   */
  createIoMatrixEntry($entry, sType, sensor) {
    $entry.addClass(sType);
    $entry.find(".th").html(sensor.name);
    $entry.attr("data-target", sensor.idx);
    $entry.attr("id", "iome_" + sensor.idx);
    if (sensor.on) {
      $entry.addClass("on");
    }
    if (sensor.email) {
      $entry.find(".td.conn_email i").addClass("on");
    }
    if (sensor.snmp) {
      $entry.find(".td.conn_snmp i").addClass("on");
    }
    return $entry;
  }

  /**
   * Gets sensor by id
   * @param {type} idx
   * @returns {unresolved}
   */
  getSensor(idx) {
    let res = null;
    this.sensorTypes.forEach((sType, index) => {
      sType.sensors.forEach((sensor) => {
        if (idx === sensor.idx) {
          res = sensor;
        }
      });
    });
    return res;
  }

  /**
   * Method that handles form submits and calls the correspondent action method
   * @param {type} section
   * @param {type} action
   * @param {type} json
   * @returns {undefined}
   */
  formHandler(section, action, json) {
    switch (section) {
      case "sensors":
        (action === "save") && this.saveSensors();
        (action === "simulate") && this.toggleSimulation();
        break;

      case "configuration": this.settings.configuration.save(json);     break;
      case "email"        : this.settings.email.submit(action, json);   break;
      case "snmp"         : this.settings.snmp.save(json);              break;
      case "time"         : this.settings.time.submit(action, json);    break;
      case "access"       : (action === "save") && this.uploadBinary(); break;
      default: break;
    }
  }

  uploadBinary() {
    var blobFile = $("#firmware")[0].files[0];
    if (!blobFile) {
      alert("No file selected!");
    } else {
      var reader = new FileReader();
      reader.readAsArrayBuffer(blobFile);
      reader.onload = function() {
        var arrayBuffer = reader.result
        var bytes = new Uint8Array(arrayBuffer);

        var oReq = new XMLHttpRequest();
        oReq.open("POST", app.serverIp + "file_upload/" + blobFile.name, true);
        oReq.onload = function (oEvent) {};
        oReq.send(bytes);
      }
    }
  }

  /**
   * Method for updating the sensor data from UI
   * @returns {undefined}
   */
  updateSensors() {
    this.sensorTypes.forEach((sType, index) => {
      sType.sensors.forEach((sensor) => {
        const $s = $("#" + sensor.idx);
        const json = $s.find("form").serializeArray();
        //console.log("jzon " + JSON.stringify(json));
        console.log(json[0].name + " " + json[0].value + " ; " + json[4].name + " " + json[4].value + " ; " + json[5].name + " " + json[5].value);
        json.forEach((elem) => {
          if (elem.name === "email" || elem.name === "snmp") {
            sensor[elem.name] = elem.value;
          } else if (elem.name === "status") {
            sensor["on"] = elem.value;
            sensor["status"] = elem.value;
          } else {
            sensor[elem.name] = elem.value;
          }
        });
      });
    });
  }

  /**
   * Method that saves sensors to server
   * @returns {undefined}
   */
  saveSensors() {
    this.logger.info("Data: saving sensors");
    this.updateSensors();
    let sensorTypes = this.formatSensorsForSaving();
    const wrap = {sensors: {sensorGroups: sensorTypes}};
    saveToServer({"set": wrap});
  }

  /**
   * Formates sensors for saving
   * by removing the unwanted propertise from the cloned array
   * @returns {undefined}
   */
  formatSensorsForSaving() {
    let out = jQuery.extend(true, [], this.sensorTypes);
    out.forEach((sType, index) => {
      sType.sensors.forEach((sensor) => {
        delete sensor['value'];
        delete sensor['graph'];
        delete sensor['oGraph'];
        delete sensor['signal'];
        delete sensor['simulation'];
        delete sensor['status'];
        delete sensor['timestamp'];
        delete sensor['type'];
        delete sensor['unit'];
      });
    });
    return out;
  }

  /**
   * Toggle simulation
   * @returns {undefined}
   */
  toggleSimulation() {
    this.simulation = !this.simulation;
    if (this.simulation) {
      $("body").addClass("simulation");
    } else {
      $("body").removeClass("simulation");
    }
    this.logger.info("Data: simulation toggle: " + (this.simulation ? "on" : "off"));
    //const wrap = {"simulation": {"active": this.simulation}};
    const wrap = {"simulation": {"active": this.simulation, "fast_motion": 60, "cur_time": 600 }};
    saveToServer(wrap);
    this.simulate();
  }

  /**
   * Simulation method
   * @returns {undefined}
   */
  simulate() {}
}

/**
 * Settings class
 * @type type
 */
class Settings {
  constructor() {
    this.time = [];
    this.email = [];
    this.snmp = [];
    this.configuration = [];

    if (arguments && arguments[0]) {
      this.time = new Time(arguments[0].time);
      this.email = new Email(arguments[0].email);
      this.snmp = new Snmp(arguments[0].snmp);
      this.configuration = new Configuration(arguments[0].config);
      // console.log("SETTINGS", this);
    } else {
      this.time = new Time();
      this.email = new Email();
      this.snmp = new Snmp();
      this.configuration = new Configuration();
    }
  }

  save() {
    this.configuration.save();
    this.email.save();
    this.snmp.save();
    this.time.save();
  }
}

/**
 * Configuration class
 * @type type
 */
class Configuration {
  constructor() {
    if (arguments && arguments[0]) {
      for (var prop in arguments[0]) {
        this[prop] = arguments[0][prop];
      }
    } else {
      this.devicename = "";
      this.temperatureunit = "";
      this.periodrestart = "";
      this.webrefresh = "";
      this.dhcp = "";
      this.ipaddress = "";
      this.networkmask = "";
      this.gateway = "";
      this.dnsprimary = "";
      this.dnssecondary = "";
      this.httpport = "";
      this.username = "";
      this.password = "";
    }

    this.populate();
  }

  update(json) {
    json.forEach((elem) => {
      this[elem.name] = elem.value;
    });
  }

  populate() {
    const selector = "#configuration #";
    for (var prop in this) {
      populateValue($(selector + prop), this[prop]);
    }
  }

  save(json) {
    app.logger.info("Data: saving configuration");
    // var jsonOutput = {"output": {"type": this.type, "name": this.name, "idx": this.idx, "source": this.source, "chain": {"delay": this.chain.delay, "hysteresis": this.chain.hysteresis}}};
    if (json) {
      this.update(json);
      const wrap = {settings: {config: this}};
      saveToServer({"set": wrap});
    } else {
      app.logger.error("Data: saving configuration failed!");
    }
  }
}

/**
 * Email configuration class
 * @type type
 */
class Email {
  constructor() {
    if (arguments && arguments[0]) {
      for (var prop in arguments[0]) {
        this[prop] = arguments[0][prop];
      }
    } else {
      this.authentication = "";
      this.cc = "";
      this.from = "";
      this.importance = "";
      this.smtpPort = "";
      this.smtpServer = "";
      this.subject = "";
      this.ssl = "";
      this.tls = "";
      this.to = "";
    }
    this.populate();
  }

  populate() {
    const selector = "#email #";
    for (var prop in this) {
      populateValue($(selector + prop), this[prop]);
    }
  }

  submit(action, json) {
    switch (action) {
      case "save":
        this.save(json);
        break;

      case "test":
        this.sendtest();
        break;

      default:

        break;
    }
  }

  update(json) {
    if (json) {
      json.forEach((elem) => {
        this[elem.name] = elem.value;
      });
    }
  }

  save(json) {
    app.logger.info("Data: saving email configuration");
    if (json) {
      this.update(json);
      const wrap = {settings: {email: this}};
      saveToServer({"set": wrap});
    } else {
      app.logger.error("Data: saving email configuration failed!");
    }
  }

  sendtest() {
    app.logger.info("Data: sending test email");
    $.ajax({
      url: app.serverIp,
      data: JSON.stringify({"test_email" : "test"}),
      method: "POST",
      dataType: 'json'
    }).done(function (data) {
      app.logger.info("Sending test e-mail successful!");
    }).fail(function (data, status, error) {
      app.logger.error("Sending test e-mail failed!");
      alert("Sending test e-mail failed!");
    }).always(function (data) {
      $('.configuration form').removeClass("processing");
      //console.log("always", data);
    });
  }
}

/**
 * Snmp configuration class
 * @type type
 */
class Snmp {
  constructor() {
    if (arguments && arguments[0]) {
      for (var prop in arguments[0]) {
        this[prop] = arguments[0][prop];
      }
    } else {
      this.aprivate = "";
      this.apublic = "";
      this.contact = "";
      this.location = "";
      this.name = "";
      this.port = "";
    }
    this.populate();
  }

  update(json) {
    if (json) {
      json.forEach((elem) => {
        this[elem.name] = elem.value;
      });
    }
  }

  populate() {
    const selector = "#snmp #";
    for (var prop in this) {
      populateValue($(selector + prop), this[prop]);
    }
  }

  save(json) {
    app.logger.info("Data: saving snmp configuration");
    if (json) {
      this.update(json);
      const wrap = {settings: {snmp: this}};
      saveToServer({"set": wrap});
    } else {
      app.logger.error("Data: saving snmp configuration failed!");
    }
  }
}

/**
 * Time configurations class
 * @type type
 */
class Time {
  constructor() {
    if (arguments && arguments[0]) {
      for (var prop in arguments[0]) {
        this[prop] = arguments[0][prop];
      }
    } else {
      this.server = "";
      this.timezone = "";
      this.interval = "";
      this.summertime = "";
      this.datetime = "";
    }
    this.populate();
  }

  populate() {
    const selector = "#time #";
    for (var prop in this) {
      populateValue($(selector + prop), this[prop]);
    }
  }

  submit(action, json) {
    switch (action) {
      case "save":
        this.save(json);
        break;

      case "sync":
        this.synchronize();
        break;

      default:

        break;
    }
  }

  update(json) {
    if (json) {
      json.forEach((elem) => {
        this[elem.name] = elem.value;
      });
    }
  }

  save(json) {
    app.logger.info("Data: saving time configuration");
    if (json) {
      this.update(json);
      const wrap = {settings: {time: this}};
      saveToServer({"set": wrap});
    } else {
      app.logger.error("Data: saving time configuration failed!");
    }
  }

  synchronize() {
    app.logger.info("Data: synchronizing sntp");
    const wrap = {time: "sync"};
    saveToServer({"set": wrap});
  }
}

/**
 * SensorType class
 */
class SensorType {

  constructor() {
    if (arguments) {
      this.name = arguments[0];
      this.unit = arguments[1];
    } else {
      this.name = "";
      this.unit = "";
    }
    this.sensors = [];
  }

  setSensors(sens) {
    this.sensors = sens;
  }
}

/* 
 * Sensor class
 */
class Sensor {

  constructor() {
    if (arguments) {
      this.type = arguments[0];
      this.idx = arguments[1].idx;
      this.name = arguments[1].name;
      this.hysteresis = arguments[1].hysteresis;
      this.min = arguments[1].min;
      this.max = arguments[1].max;
      this.on = arguments[1].on;
      this.email = arguments[1].email;
      this.snmp = arguments[1].snmp;
      this.port = arguments[1].port;
    } else {
      this.type = null;
      this.idx = 0;
      this.name = "";
      this.hysteresis = 0;
      this.min = 0;
      this.max = 0;
      this.on = 0;
      this.email = 0;
      this.snmp = 0;
      this.port = "";
    }
    this.value = null;
    this.signal = null;
    this.simulation = 0;
    this.status;
    this.timestamp = 0;
    this.graph = null;
    this.oGraph = null;
    this.uiElement;
    this.ioMatrixRow;
  }

  /**
   * Creates UI block for the Sensor, 
   * based on the provided template and SensorType, 
   * and add it to the provided pane/container
   * @param {type} $template
   * @param {type} sensorType
   * @param {type} $sensorTypePane
   * @returns {undefined}
   */
  createUi($template, sensorType, $sensorTypePane) {
    let $sens = $template.clone();
    $sens.removeClass("hidden");
    $sens.attr("id", this.idx);
    $sens.addClass(sensorType);
    $sens.find("form .name").val(this.name);
    $sens.find("h3 span").html(this.name);
    $sens.find("h3 i").attr("class", getSensorTypeIcon(sensorType));
    $sens.find("form .hysteresis").val(this.hysteresis);
    $sens.find("form .min").val(this.min);
    $sens.find("form .max").val(this.max);
    $sens.find("form .name").val(this.name);
    $sens.find("form .status ").val(this.on);
    $sens.find("form .email").val(this.email);
    $sens.find("form .snmp").val(this.snmp);
    if (this.email) {
      $sens.find("form .email_toggle").addClass("on");
    }
    if (this.snmp) {
      $sens.find("form .snmp_toggle").addClass("on");
    }
    $sens.data(this);
    this.graph = $sens.find(".graph_panel");
    this.oGraph = $("#type_overview .sensor#" + this.idx + " .graph .graph_panel");
    // this.uiElement = $sens;
    $sensorTypePane.append($sens);
    // trigger on toggle after the sensor is added
    if (this.on) {
      $sens.find("form .status_toggle").click();
    }
    // console.log($sens.data());
    app.logger.info("* Data: Sensor loaded: " + this.name);
  }

  /**
   * Sets the status value and updates the ui elements
   * @param {type} val
   * @returns {undefined}
   */
  setStatus(val) {
    this.status = val;
    this.on = this.status;
    $("#" + this.idx + " input.status").val(val);
    if (val) {
      $("#" + this.idx + " input.status").parent().addClass("on");
      $("#" + this.idx).addClass("on");
      $("#ioMatrix #iome_" + this.idx).addClass("on");
    } else {
      $("#" + this.idx + " input.status").parent().removeClass("on");
      $("#" + this.idx).removeClass("on");
      $("#ioMatrix #iome_" + this.idx).removeClass("on");
    }
    this.operate();
  }

  /**
   * Sets the email value and updates the ui elements
   * @param {type} val
   * @returns {undefined}
   */
  setEmail(val) {
    this.email = val;
    $("#" + this.idx + " input.email").val(val);
    if (val) {
      $("#" + this.idx + " input.email").parent().addClass("on");
      $("#ioMatrix #iome_" + this.idx + " .conn_email .conn").addClass("on");
    } else {
      $("#" + this.idx + " input.email").parent().removeClass("on");
      $("#ioMatrix #iome_" + this.idx + " .conn_email .conn").removeClass("on");
    }
  }

  /**
   * Sets the snmp value and updates the ui elements
   * @param {type} val
   * @returns {undefined}
   */
  setSnmp(val) {
    this.snmp = val;
    // update UI elements, sensor and io matrix
    $("#" + this.idx + " input.snmp").val(val);
    if (val) {
      $("#" + this.idx + " input.snmp").parent().addClass("on");
      $("#ioMatrix #iome_" + this.idx + " .conn_snmp .conn").addClass("on");
    } else {
      $("#" + this.idx + " input.snmp").parent().removeClass("on");
      $("#ioMatrix #iome_" + this.idx + " .conn_snmp .conn").removeClass("on");
    }
  }

  setName(val) {
    this.name = val;
    // update UI elements, sensor and io matrix
    $("#" + this.idx + " input.name").val(val);
    $("#ioMatrix #iome_" + this.idx + " .th").val(val);
  }

  setHysteresis(val) {
    this.hysteresis = val;
  }

  setMin(val) {
    this.min = val;
  }

  setMax(val) {
    this.max = val;
  }

  /**
   * saves the sensor by sending it to server
   * @return {undefined}
   */
  save() {
    const jsonInput = {"input": {"type": this.type, "name": this.name, "idx": this.idx, "para": this.para}};
    console.log("saving input\n", jsonInput);
    saveToServer({"set": jsonInput});
  }

  /**
   * 
   * @return {undefined}
   */
  fetch() {
    const obj = this;
    let reqObj = {"fetch": {"idx": this.idx}};
    if (obj.status) {
      $.ajax({
        url: app.serverIp,
        method: "POST",
        data: JSON.stringify(reqObj),
        dataType: 'json'
      }).done(function (data) {
        // console.log(obj, obj.signal);
        if (data.error==='false' || data.error==='0') {
          obj.value = data.values[0];
          data.message;
          obj.timestamp = data.timestamp;
        }
        // console.log("input fetch success");
      }).error(function (data, status, error) {
        console.log(error);
        obj.value = -1;
        // client side simulation 
        if (app.simulation) {
          // obj.value = (obj.inputType != null) ? obj.inputType.generateOutput() : -1;
        }
      }).always(function (data) {
        obj.draw();
      });
    }
  }

  /**
   * 
   * @return {undefined}
   */
  draw() {
    if (this.graph && this.status) {
      let $val;
      if (this.value < 0) {
        $val = $('<div class="sensor_value no_signal">');
        $val.css("height", 0);
      } else {
        $val = $('<div class="sensor_value">');
        // let h = 100 * this.value / app.getSensor(this.type).max;
        let h = 100 * this.value / this.max;
        $val.css("height", h + "%");
      }
      if (app.simulation) {
        $val.addClass("sim");
      }
      $(this.graph).append($val);
      this.updateOverview($val.clone());
      // $(this.graph).hide();
      let num = $(this.graph).find(".sensor_value").length;
      if (num > noOfSamples) {
        $(this.graph).find(".sensor_value").first().remove();
      }
    }
  }

  /**
   * Method for updating the data and graph on the overview panel
   * @param {type} $elem
   * @returns {undefined}
   */
  updateOverview($elem) {
    let $oSensorVal = $("#type_overview .sensor#ov-" + this.idx + " .value");
    if (this.oGraph.length > 0) {
      $(this.oGraph).append($elem);
      let num = $(this.oGraph).find(".sensor_value").length;
      if (num > noOfSamples * 2) {
        $(this.oGraph).find(".sensor_value").first().remove();
      }
      $oSensorVal.html(Math.round(this.value * 100) / 100);
    } else {
      this.oGraph = $("#type_overview .sensor#ov-" + this.idx + " .graph .graph_panel");
      this.updateOverview($elem);
    }
  }

  /**
   * 
   * @return {undefined}
   */
  activate() {
    this.fetch();
    if (this.type) {
      let obj = this;
      $(this.graph).html("");
      saveToServer({"sensor":{"idx":this.idx,"":{"on":this.on}}});
      this.signal = setInterval(function () {
        obj.fetch();
      }, app.sensorFetchInterval);
    }
  }

  /**
   * 
   * @return {undefined}
   */
  deactivate() {
    saveToServer({"sensor":{"idx":this.idx,"":{"on":this.on}}});
    clearInterval(this.signal);
    this.signal = null;
  }

  /**
   * 
   * @return {undefined}
   */
  operate() {
    if (this.status) {
      this.activate();
    } else {
      this.deactivate();
    }
  }
}

/**
 * Logger class
 * Handles all application logging in UI
 * @type type
 */
class Logger {

  /**
   * Constructor method
   * pass parameter array in the following order:
   * arg0: logToConsole
   * arg1: logToUI
   * arg2: UI logger selector
   * @returns {Log}
   */
  constructor() {
    this.logToConsole = true;
    this.logToUI = true;
    this.uiLogger;
    this.tempStack = [];

    if (arguments) {
      this.logToConsole = arguments[0];
      this.logToUI = arguments[1];
      this.uiLogger = arguments[2];
    } else {
      this.name = "";
    }
  }

  /**
   * info log method
   * @param {type} message
   * @returns {undefined}
   */
  info(message) {
    this._log(message, "info");
  }

  /**
   * error log method
   * @param {type} message
   * @returns {undefined}
   */
  error(message) {
    this._log(message, "error");
  }

  /**
   * warning log method
   * @param {type} message
   * @returns {undefined}
   */
  warning(message) {
    this._log(message, "warning");
  }

  /**
   * private generic log method
   * @param {type} message
   * @param {type} messageType
   * @returns {undefined}
   */
  _log(message, messageType) {
    this._logToUI(message, messageType);
    this._logToConsole(message, messageType);
  }

  _logToUI(message, messageType) {
    if (this.logToUI) {

      let $m = $("<div>").addClass("log_message").addClass(messageType).html(this._getDateTime() + "&nbsp;&nbsp;&nbsp;" + message);
      if (this.uiLogger) {
        if ($(this.uiLogger).length > 0) {
          // if the ui component is present, print and empty stack first
          this._printAndEmptyStack();
          // then print out the message
          $(this.uiLogger).append($m);
        } else {
          // if ui component is not there yet, push to stack
          this.tempStack.push($m);
        }
      } else {
        this.tempStack.push($m);
      }
    }
  }

  _printAndEmptyStack() {
    if (this.uiLogger) {
      while (this.tempStack.length > 0) {
        $(this.uiLogger).append(this.tempStack.shift());
      }
    }
  }

  _logToConsole(message, messageType) {
    if (this.logToConsole) {
      console.log(messageType, message);
    }
  }

  _getDateTime() {
    const dt = new Date();
    const day = (dt.getDay() > 9) ? dt.getDay() : "0" + dt.getDay();
    const mnth = (dt.getMonth() + 1 > 9) ? (dt.getMonth() + 1) : "0" + (dt.getMonth() + 1);
    let millis = (dt.getMilliseconds() / 10).toFixed(0);
    millis = (millis < 10) ? ("0" + millis) : millis;
    const time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds() + ":" + millis;
    return dt.getFullYear() + "-" + mnth + "-" + day + " " + time;
  }

}
