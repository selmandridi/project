#ifndef CRC16_H_   
#define CRC16_H_

#include <stdio.h>
#include "ff.h"
#include "stdint.h"

unsigned short crc16(const unsigned char* data_p, unsigned char length, unsigned short crc);

int check_crc(void);

int decompress_packed_file(void);

FRESULT removedir(char* path);


#endif // CRC16_H_
