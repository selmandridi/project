#include "crc16.h"

/*
// this is the CCITT CRC 16 polynomial  0x1021
*/

unsigned short crc16(const unsigned char* data_p, unsigned char length, unsigned short crc){
    unsigned char x;

    while (length--){
        x = crc >> 8 ^ *data_p++;
        x ^= x>>4;
        crc = (crc << 8) ^ ((unsigned short)(x << 12)) ^ ((unsigned short)(x <<5)) ^ ((unsigned short)x);
    }
    return crc;
}


int check_crc(void)
{
    int bloc_size = 255;
    int counter = 0;

    FIL fsrc, fdst;      /* File objects */
    BYTE buff[49980], buff_tmp[bloc_size];  /* Buffer for the file name */
    BYTE crc[2];  /* Buffer for the file name */
    uint16_t crcNbre=0;

    FRESULT fr;          /* FatFs function common result code */
    UINT br, bw;         /* File read/write count */

    unsigned short value;

    uint16_t crc_ccitt_ffff_val_tmp, crc_ccitt_ffff_val = 0xffff;

    int fsize = 0;


    /* Open source file */
    fr = f_open(&fsrc, "generated_crc.bin", FA_READ);
    if (fr) return (int)fr;


    fsize = f_size(&fsrc);


    while(fsize)
    {

        memset(&buff[0], NULL, sizeof(buff));

        if(fsize < 49980)

            fr = f_read(&fsrc, buff, fsize, &br);  /* Read a chunk of source file */
        else
            fr = f_read(&fsrc, buff, 49980, &br);  /* Read a chunk of source file */


        if (fr || br == 0) /* error or eof */
        {

            crcNbre = crcNbre +  buff_tmp[strlen(buff_tmp)-1];
            crcNbre = crcNbre +  256 * buff_tmp[strlen(buff_tmp)-2];

            crc_ccitt_ffff_val = crc16(buff_tmp,strlen(buff_tmp)-2,crc_ccitt_ffff_val_tmp);

            break;
        }

        for(int i=0;i < 196; i++)
        {
            if((fsize - (i * 255)) < 0)
                break;

            memcpy(buff_tmp,buff+(i*255),255);

            crc_ccitt_ffff_val_tmp = crc_ccitt_ffff_val;

            crc_ccitt_ffff_val = crc16(buff_tmp,255,crc_ccitt_ffff_val);
        }

        fsize = fsize - 49980;

    }

    f_close(&fsrc);

    printf("CRC calculated %d CRC expected:%d\n",crc_ccitt_ffff_val,crcNbre);

    if(crc_ccitt_ffff_val == crcNbre)
        return 1;
    else
        return 0;
}



int decompress_packed_file(void)
{ 
    FIL fsrc, fdst;            /* File objects */
    BYTE buffer[50000];            /* File copy buffer */
    BYTE size[4],sizepath[2];  /* Buffer for the file size */
    BYTE filename[100] = {0};  /* Buffer for the file name */
    BYTE tmpfilename[100] = {0};  /* Buffer for the file name */
    
    FRESULT fr;                /* FatFs function common result code */
    UINT br, bw;               /* File read/write count */
    FILINFO fno;               /* Fatfs FILINFO structure */
 
    if(check_crc())
    {    
        /* Open source file */
        fr = f_open(&fsrc, "generated_crc.bin", FA_READ);
        if (fr) return (int)fr;

        int parameter=0, root_dir=1;

        uint32_t filesize=0, pathsize=0;

        for (;;) {

            switch(parameter)
            {
                case 0:

                    //printf("path size extracting\n");

                    pathsize = 0;               

                    fr = f_read(&fsrc, sizepath, 2, &br);  /* Read a chunk of source file */
                    if (fr || br == 0) { f_close(&fsrc); return 0;} /* error or eof */

                    pathsize = pathsize +  sizepath[1];
                    pathsize = pathsize +  256 * sizepath[0];

                    parameter = 1 ;

                    //printf("path size : %d\n", pathsize );

                    break;

                case 1:

                    //printf("path  extracting\n");

                    memset(&filename[0], 0, sizeof(filename));

                    fr = f_read(&fsrc, filename, pathsize, &br);  /* Read a chunk of source file */
                    if (fr || br == 0) { f_close(&fsrc); return 0;} /* error or eof */

                    //printf("%s\n",filename );     

                    for(int i=0; i< pathsize; i++)
                    {
                        //printf("test %c %d\n",filename[i], filename[i]);
                        
                        if((filename[i] == 47) || (filename[i] == 92))    /* 47 and 92 is the ASCII code for "/" and "\"  */ 
                        {
                            filename[i] = 47; 

                            memset(&tmpfilename[0], 0, sizeof(tmpfilename));

                            memcpy(tmpfilename, filename, i);

                            //printf("filename: %s\n",filename);

                            if(root_dir)
                            {
                                if(f_stat(tmpfilename, &fno) == 0)
                                {
                                    //printf("root %s\n",tmpfilename);

                                    removedir(tmpfilename);
                                }
                                root_dir =0;
                            }

                            f_mkdir(tmpfilename);                           
                        }                 
                    }                       

                    parameter =2;

                    fr = f_open(&fdst, filename, FA_WRITE);
                    if (fr) return (int)fr;

                    break;

                case 2:

                    //printf("data size extracting\n");

                    filesize = 0;

                    fr = f_read(&fsrc, size, 4, &br);  /* Read a chunk of source file */
                    if (fr || br == 0) { f_close(&fsrc); return 0;} /* error or eof */

                    filesize = filesize +  size[3];
                    filesize = filesize +  256 * size[2];
                    filesize = filesize +  65536 * size[1];
                    filesize = filesize +  16777216* size[0];

                    //printf("file size : %d\n", filesize );

                    parameter = 3 ;
                    
                    break;

                case 3:

                    if ( filesize > 50000)  /* use the 50kb of the RAM */
                    {
                        fr = f_read(&fsrc, buffer, 50000, &br);  /* Read a chunk of source file */

                        filesize = filesize - 50000;
                    }    
                    else
                    {
                        fr = f_read(&fsrc, buffer, filesize, &br);  /* Read a chunk of source file */

                        filesize = 0;
                    }   

                    if (fr || br == 0) { f_close(&fsrc); return 0;} /* error or eof */  
                        
                    if (filesize > 0)
                    {                   
                        fr = f_write(&fdst, buffer, br, &bw);            /* Write it to the destination file */
                        if (fr || bw < br) { f_close(&fdst); return 0;} /* error or eof */
                    }
                    else
                    {
                        fr = f_write(&fdst, buffer, br, &bw);            /* Write it to the destination file */
                        if (fr || bw < br) { f_close(&fdst); return 0;} /* error or eof */
                            
                        f_close(&fdst);

                        parameter = 0 ;
                    }
                    break;
            }           
        }

        /* Close open files */
        f_close(&fsrc);

        return (int)fr;
    }
}

FRESULT removedir(
    char* path      /* Working buffer filled with start directory */
)
{
    FRESULT fr;
    DIR dir;
    FILINFO fno = {0};
    char buff[256] = {0};

#if _USE_LFN
    fno.lfname = 0; /* Disable LFN output */
#endif
    fr = f_opendir(&dir, path);
    if (fr == FR_OK)
    {
        for (;;)
        {
            memset(&buff[0], 0, sizeof(buff));

            memset(&fno.fname[0], 0, sizeof(&fno.fname));

            fno.fattrib = 0;

            fr = f_readdir(&dir, &fno);
            if (fr != FR_OK || !&fno.fname[0]) break;

            if (fno.fname[0] == '.' )
                continue;

            sprintf(buff,"%s/%s",path,&fno.fname);

            if (fno.fattrib & AM_DIR)
            {
                fr = f_unlink(buff);
                if (fr != FR_OK)
                {
                    fr = removedir(buff);
                    if (fr != FR_OK) continue;
                }
                continue;
            }

            fr = f_unlink(buff);
            if (fr != FR_OK) break;
        }
        f_closedir(&dir);
    }

    return fr;
}

