//#include "globals.h"

#include "ff.h"
#include <sys/stat.h>

#include <stdio.h>
#include <unistd.h>

#include <string.h>

#ifndef UNUSED_PARAM
#define UNUSED_PARAM(param) (void)(param)
#endif

#define DEBUG_FATFS 0

#if DEBUG_FATFS
#define FATFS_DEBUG(x) {x}
#else
#define FATFS_DEBUG(x)
#endif

struct tFdFpMapItem {
    FIL *fp;
    FILE *fd;
};

// This map assumes no more than 20000 files will be stored. No checks are performed.
#define FD_FP_MAP_SIZE 20000
static struct tFdFpMapItem fdFpMap[FD_FP_MAP_SIZE];
static int fdFpMapTop = 0;

#define GET_FD(fp) \
    FILE *fd = getFd(fp); \
    if (fd == 0) { \
        return FR_NO_FILE; \
    } \
    FATFS_DEBUG(printf("%15s   %llX\n", __func__, (long long unsigned int)fd);)

static void setFd(FIL *fp, FILE *fd) {
    int i;
    for (i = 0; i < fdFpMapTop; i++) {
        if (fdFpMap[i].fp == fp) {
            fdFpMap[i].fd = fd;
            return;
        }
    }

    // add a new mapping if fp is not found
    fdFpMap[fdFpMapTop].fp = fp;
    fdFpMap[fdFpMapTop].fd = fd;
    fdFpMapTop++;
    FATFS_DEBUG(printf("fdFpMapTop== %d\n", fdFpMapTop);)
}

static FILE *getFd(FIL *fp) {
    int i;
    for (i = 0; i < fdFpMapTop; i++) {
        if (fdFpMap[i].fp == fp) {
            return fdFpMap[i].fd;
        }
    }
    return NULL;
}

#ifdef f_stat
#undef f_stat
#endif
FRESULT f_stat ( // this mock only checks for existence, ignores the second parameter!
        const TCHAR* path,	/* Pointer to the file path */
        FILINFO* fno		/* Pointer to file information to return */
        ) {
    struct stat b;
    if (0 != stat(path, &b)) {
        return FR_NO_FILE;
    } else {
        return FR_OK;
    }
}

#ifdef f_size
#undef f_size
#endif
DWORD f_size(
        FIL* fp   /* [IN] File object */
        ) {
    GET_FD(fp);
    struct stat st;
    if (fstat(fd->_fileno, &st) < 0) return -1;
    return st.st_size;
}

#ifdef f_tell
#undef f_tell
#endif
DWORD f_tell(
        FIL* fp   /* [IN] File object */
        ) {
    GET_FD(fp);
    return ftell(fd);
}

FRESULT f_sync(
        FIL* fp           /* [IN] Pointer to the file object structure */
) {
    return FR_OK;
}

FRESULT f_write(
        FIL* fp,          /* [IN] Pointer to the file object structure */
        const void* buff, /* [IN] Pointer to the data to be written */
        UINT btw,         /* [IN] Number of bytes to write */
        UINT* bw          /* [OUT] Pointer to the variable to return number of bytes written */
        ) {
    GET_FD(fp);
    UINT lbw = fwrite(buff, 1, btw, fd);
    if (bw) *bw = lbw;
    fflush(fd);
    return FR_OK;
}

FRESULT f_read(
        FIL* fp,     /* [IN] File object */
        void* buff,  /* [OUT] Buffer to store read data */
        UINT btr,    /* [IN] Number of bytes to read */
        UINT* br     /* [OUT] Number of bytes read */
        ) {
    GET_FD(fp);
    *br = fread(buff, 1, btr, fd);
    return FR_OK;
}

FRESULT f_open(
        FIL* fp,           /* [OUT] Pointer to the file object structure */
        const TCHAR* path, /* [IN] File name */
        BYTE mode          /* [IN] Mode flags */
        ) {
    FILE *fd = 0;
    int rv = FR_NO_FILE;

    char nm[256];
    if (path[0] == '/') {
        nm[0] = '.';
        strcpy(nm + 1, path);
    } else {
        nm[0] = '.';
        nm[1] = '/';
        strcpy(nm + 2, path);
    }

    switch (mode) {
        case FA_OPEN_EXISTING:
            if( access( nm, F_OK ) != -1 ) {
                if (NULL != (fd = fopen(nm, "a"))) rv = FR_OK;
            }
            break;
        case FA_WRITE  : if (NULL != (fd = fopen(nm, "w"))) rv = FR_OK; break;
        case FA_READ   : if (NULL != (fd = fopen(nm, "r"))) rv = FR_OK; break;
    case FA_OPEN_ALWAYS | FA_WRITE | FA_READ: // dirty!
        if( access( nm, F_OK ) == -1 ) {
            if (NULL != (fd = fopen(nm, "w+"))) rv = FR_OK;
        } else {
            if (NULL != (fd = fopen(nm, "r+"))) rv = FR_OK;
        }
        break;            
            break;
    }
    if (rv == FR_OK) {
        setFd(fp, fd);
        FATFS_DEBUG(printf("%15s   %llX %X %s\n", __func__, (long long unsigned int)fd, mode, nm);)
        fp->fsize = f_size(fp);
    }
    return rv;
}

FRESULT f_close(
        FIL* fp     /* [IN] Pointer to the file object */
        ) {
    GET_FD(fp);
    fclose(fd);
    setFd(fp, NULL);
    return FR_OK;
}


FRESULT f_unlink(
        const TCHAR* path  /* [IN] Object name */
        ) {

    if(!unlink(path)) return FR_OK;

    if(!rmdir(path)) return FR_OK;
    
    return FR_DENIED;
}


FRESULT f_mount(FATFS* fs, const TCHAR* path, BYTE opt) {
    UNUSED_PARAM(fs);
    UNUSED_PARAM(path);
    UNUSED_PARAM(opt);
    return FR_OK;
}

FRESULT f_lseek(FIL* fp, DWORD ofs) {
    GET_FD(fp);
    int res = fseek(fd, ofs, SEEK_SET);
    return FR_OK;
}


FRESULT f_mkdir (const TCHAR* path)
{   
    mkdir(path, 0777);    
    
    return FR_OK;
}


struct dirent {
    ino_t          d_ino;       /* inode number */
    off_t          d_off;       /* offset to the next dirent */
    unsigned short d_reclen;    /* length of this record */
    unsigned char  d_type;      /* type of file; not supported
                                   by all file system types */
    char           d_name[256]; /* filename */
};

FRESULT f_opendir (
  DIR** dp,           /* [OUT] Pointer to the directory object structure */
  const char* path  /* [IN] Directory name */
)
{
    *dp = opendir(path) ;

    if(&dp == NULL)
    {
      printf("ERROR: Couldn't open directory.\n");
      return 1;
    }
    
    return FR_OK;
}

FRESULT f_readdir (
    DIR* dp,            /* Pointer to the open directory object */
    FILINFO* fno        /* Pointer to file information to return */
)
{
    struct dirent* fichierLu; /* Déclaration d'un pointeur vers la structure dirent. */
       
    if (dp == NULL)
        return(1);

    dp = dp->fs;


    fichierLu = readdir(dp);

    if(fichierLu == NULL)
        return 0;

    strcpy(&fno->fname,&fichierLu->d_name);

    if(fichierLu->d_type == 4)
        fno->fattrib = AM_DIR;
    else
        fno->fattrib = 0;

    return 0;
}


FRESULT f_closedir (
  DIR** dp     /* [IN] Pointer to the directory object */
)
{
    closedir(*dp);

    return FR_OK;


}


