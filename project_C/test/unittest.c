#include <stdio.h>
#include <assert.h>

#include "crc16.h"


#define run_test(fn_name) \
    printf("\ntest: %s\n", #fn_name); \
    fn_name()

// Stupid sample tests
void test_check_crc()
{
	int result=0;

	result = check_crc();
    assert(result == 1);
}

void test_unpacking()
{
	int result=0;

	result = decompress_packed_file();
    assert(result == 0);
}


int main(int argc, char *argv[])
{

    run_test(test_check_crc);
    
    printf("\nTest check crc OK\n");

    run_test(test_unpacking);

    printf("\nTest decompress paccked file OK\n");

    
    return 0;
}