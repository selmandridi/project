import os
from binascii import hexlify
from functools import partial
import ctypes
import crc16

def check_directory():
    """
    This function to check if the folder exist 
    
    """
    folder = input('Choose a path of the folder:')

    if folder[0] != '.' or folder[0] != '/'  :
        folder = "./" + folder

    if (os.path.isdir(folder)):
        return(folder)
    else:
        print(" This folder doesn't exit")
        check_directory()


def convertion_function(directory):
    """
    This function will convert the directory in a text file 
    
    """
    f = open('tmp.bin', 'wb')

    if(os.path.isabs(directory)):
        print("It is an absotute path")        
               
    else:
        print("It is a relative path")
        root_ = directory[2:len(directory)]   

    for root, directories, files in os.walk(directory):
        for filename in files:
            # Join the two strings in order to form the full filepath.
            filepath =os.path.join(root, filename)            
            
            #data = binary_converter(filepath)

            size = os.path.getsize(filepath)



            if(os.path.isabs(directory)):
                root_ = directory.split("/")

                if directory[len(directory)-1] == "/":
                    root_ = root_[len(root_)-2]

                    f.write(  (len(str(filepath[(len(directory)-len(root_)-1):len(filepath)]))).to_bytes(2, byteorder='big', signed=False) + bytes(str(filepath[(len(directory)-len(root_)-1):len(filepath)]), 'utf-8') 
                       + (size).to_bytes(4, byteorder='big', signed=False)) 
             
                    with open(filepath,'rb') as f1:
                        while True:
                            buf=f1.read(8)
                            if buf: 
                                for byte in buf:
                                    pass   

                                n=f.write(buf)
                            else:
                                break
                else:
                    
                    root_ = root_[len(root_)-1]

                    f.write( len(str(filepath[(len(directory)-len(root_)):len(filepath)])).to_bytes(2, byteorder='big', signed=False)   + bytes(str(filepath[(len(directory)-len(root_)):len(filepath)]), 'utf-8') 
                        + (size).to_bytes(4, byteorder='big', signed=False))
                   
                    with open(filepath,'rb') as f1:
                        while True:
                            buf=f1.read(8)
                            if buf: 
                                for byte in buf:
                                    pass   

                                n=f.write(buf)
                            else:
                                break

            else:
                print("path lenght: ",len(str(filepath[2:len(filepath)])))
                print("path name: ",str(filepath[2:len(filepath)]))
                print("file size: ",size)
                f.write( len(str(filepath[2:len(filepath)])).to_bytes(2, byteorder='big', signed=False) + bytes(str(filepath[2:len(filepath)]), 'utf-8')
                 + (size).to_bytes(4, byteorder='big', signed=False))
                
                with open(filepath,'rb') as f1:
                    while True:
                        buf=f1.read(8)
                        if buf: 
                            for byte in buf:
                                pass   

                            n=f.write(buf)
                        else:
                            break
    f.close()
               
    print("Convertion Done!\n")

    return(0)

def generate_crc():

    f3 = open('generated_crc.bin', 'wb')

    with open('tmp.bin','rb') as f2:

        buf=f2.read(50000)

        crc = crc16.crc16xmodem(buf,0xffff)

        print(hex(crc),crc)

        f3.write(buf)          

        while True:
            buf=f2.read(50000)
            if buf: 
                for byte in buf:
                    pass   
                crc = crc16.crc16xmodem(buf,crc)

                print(hex(crc),crc)

                f3.write(buf)            

                #f3.write((crc).to_bytes(2, byteorder='big', signed=False))            
            else:
                break

    f3.write((crc).to_bytes(2, byteorder='big', signed=False))


    print("Final CRC:",hex(crc),crc)

                
    f2.close()
    f3.close()

    return(0)

if __name__ == "__main__":

    dir = check_directory()

    convertion_function(dir)
   
    print (" Generating the CRC!\n")

    generate_crc()

    if os.path.exists("tmp.bin"):
    	os.remove("tmp.bin")

   
