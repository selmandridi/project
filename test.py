import random
import unittest
import sys,os
from io import TextIOWrapper, BytesIO
from script import convertion_function,generate_crc, check_directory


class ProjectTest(unittest.TestCase):

    def test_create_bin(self):
        dir = check_directory()
        result = convertion_function(dir)
        self.assertEqual(result,0)

    def test_create_crc(self):
        result = generate_crc()
        self.assertEqual(result,0)

if __name__ == '__main__':
    unittest.main()